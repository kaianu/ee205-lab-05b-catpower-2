///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.h
/// @version 1.0
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date 05_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

const double CATPOWER_IN_A_JOULE = 0;
const char CAT_POWER     = 'c';

extern double fromCatPowerToJoule( double catPower );
extern double fromJouleToCatPower( double joule );

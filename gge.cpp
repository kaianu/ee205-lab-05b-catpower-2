///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date 05_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "gge.h"

double fromGGEToJoule( double GGE){
   return GGE / GGE_IN_A_JOULE;
}

double fromJouleToGGE( double joule ){
   return joule * GGE_IN_A_JOULE;
}

